use std::f64;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use yew::prelude::*;
extern crate photon_rs;

enum Msg {
    AddOne,
    Render,
}

struct Model {
    value: i64,
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            value: 0,
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::AddOne => {
                self.value += 1;
                // the value has changed so we need to
                // re-render for it to appear on the page
                true
            },
            Msg::Render => {
                self.render_canvas();
                false
            }
        }
    }

    fn rendered(&mut self, ctx: &Context<Self>, first_render: bool) {
        if first_render {
            ctx.link().send_message(Msg::Render);
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        // This gives us a component's "`Scope`" which allows us to send messages, etc to the component.
        let link = ctx.link();
        html! {
            <div>
                <button onclick={link.callback(|_| Msg::AddOne)}>{ "+1" }</button>
               // <button onclick={link.callback(|_| Msg::Render)}>{ "Render Canvas" }</button>
                <p>{ self.value }</p>
                <canvas id="canvas" />
            </div>
        }
    }
}

// Our own methods
impl Model {
    fn render_canvas(&mut self) {

        println!("RENDER CANVAS");

        let document = web_sys::window().unwrap().document().unwrap();
        let canvas = document.get_element_by_id("canvas").unwrap();
        let canvas: web_sys::HtmlCanvasElement = canvas
                .dyn_into::<web_sys::HtmlCanvasElement>()
                .map_err(|_| ())
                .unwrap();

        let context = canvas
            .get_context("2d")
            .unwrap()
            .unwrap()
            .dyn_into::<web_sys::CanvasRenderingContext2d>()
            .unwrap();

        // Create image element
        let image = document.create_element("img")
            .unwrap()
            .dyn_into::<web_sys::HtmlImageElement>()
            .unwrap();

        // Set image src
        image.set_src("img/image.jpg");

        image.set_onload()


        // Draw image to canvas
        // context.draw_image_with_html_image_element(&image, 0.0, 0.0).unwrap();

        // context.begin_path();
        //
        // // Draw the outer circle.
        // context
        //     .arc(75.0, 75.0, 50.0, 0.0, f64::consts::PI * 2.0)
        //     .unwrap();
        //
        // // Draw the mouth.
        // context.move_to(110.0, 75.0);
        // context.arc(75.0, 75.0, 35.0, 0.0, f64::consts::PI).unwrap();
        //
        // // Draw the left eye.
        // context.move_to(65.0, 65.0);
        // context
        //     .arc(60.0, 65.0, 5.0, 0.0, f64::consts::PI * 2.0)
        //     .unwrap();
        //
        // // Draw the right eye.
        // context.move_to(95.0, 65.0);
        // context
        //     .arc(90.0, 65.0, 5.0, 0.0, f64::consts::PI * 2.0)
        //     .unwrap();
        //
        // context.stroke();
    }
}

fn main() {
    yew::start_app::<Model>();
}
